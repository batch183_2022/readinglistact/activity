//Start for Object data type 
let person = {
	firstName: "Juan",
	lastName: "Dela Cruz",
	age: "21",
	address: {
		city: "Quezon City",
		province: "Metro Manila"
	}
};

console.log("First Name: " + person.firstName);
console.log("Last Name: " + person.lastName);
console.log("Age: " + person.age);
console.log("Address:");
console.log("City/Municipality: " +person.address.city);
console.log("Province: " +person.address.province);

// Start for function return 
let firstName = "Juan";
let lastName = "Dela Cruz";
let age = "21";
let currentAddress = "Quezon City, Metro Manila.";


function returnFullName(firstName, lastName, age, currentAddress){
	return firstName +' '+lastName +' '+age+ ''+currentAddress;
};

let completeName = returnFullName(firstName, lastName, age, currentAddress);
console.log("Hello! I am " +firstName +' '+lastName +' '+age+ ' years old, and currently living in '+currentAddress);

// Start for Conditional Statement
function testNum(a) {
  let age = 17;
  if ( a > 17) {
    result = "You are qualified to vote!";
  } else {
    result = 'Sorry, You are too young to vote!';
  }
  return result;
}
console.log(testNum(17));


// Start for ParseInt Method
function pressed(){
	let text = parseInt(document.getElementById("inp").value);
	let output = document.getElementById("output");
		if (text == 3){
			output.innerHTML = "Valid Input!";
		}
		else if (true){
			output.innerHTML = "Invalid Input! Please enter the month number between 1-12";
		}

}
console.log("Total Number of days for March: 31");

console.log(5);
console.log(4);
console.log(3);
console.log(2);
console.log(1);
console.log(0);


